$(document).ready(function(){
	
	addProductRow();
	
	generateBarCode();
	
	$('#sender_name').change(function(){
		
		var sender_name = $(this).val();
		
		var results = $.grep(senders, function(v, i) {
		    if (v.sender_name == sender_name) {
		        return v;
		    }
		});
		
		if(results.length > 0) {
			var sender = results[results.length - 1];
		
			$("#sender_address").val(sender.sender_address);
			$("#sender_postcode").val(sender.sender_postcode);
			$("#sender_phone").val(sender.sender_phone);
		}

	});
	
	
	$('#receiver_name').change(function(){
		
		var receiver_name = $(this).val();
		
		var results = $.grep(receivers, function(v, i) {

		    if (v.receiver_name == receiver_name) {
		        return v;
		    }
		});
		
		if(results.length > 0) {
			var receiver = results[results.length - 1];
			
			$("#receiver_address_1").val(receiver.receiver_address_1);
			$("#receiver_address_2").val(receiver.receiver_address_2);
			$("#receiver_postcode").val(receiver.receiver_postcode);
			$("#receiver_phone").val(receiver.receiver_phone);
			$("#receiver_licence").val(receiver.receiver_licence);
		}

	});
	
	$(document).on('change', '.product_name', function(){
		
		var product_name = $(this).val();
		
		var results = $.grep(products, function(v, i) {

		    if (v.product_name == product_name) {
		        return v;
		    }
		});
		
		if(results.length > 0) {
		
			var product = results[results.length - 1];
			
			$(this).parent().parent().find('.product_price').val(product.product_price);
		}

	});
	
	$(document).on('change', '.product_quantity', function(){
		
		var product_quantity = $(this).val();
		
		$product_row = $(this).parent().parent();
		
		var unit_price = $product_row.find('.product_price').val();
		
		$product_row.find('.product_value').val(Number(unit_price) * Number(product_quantity));

	});
	
	$(document).on('change',"#products_table tbody tr:last input",function(){
		if($(this).val())
		{
			addProductRow();
		}
	});
	
	$('#order_number').change(function(){
		$("#barcode").attr("src", "/lib/barcode.php?text=" + $(this).val());
	});
});


function addProductRow(){
	$("#products_table tbody tr.no-display").clone().appendTo("#products_table tbody");
	$("#products_table tbody tr:last").removeClass("no-display");
}

function generateBarCode(){
	var code = Number(orders[orders.length - 1].order_number)+1;
	$("#order_number").val(code);
	$("#barcode").attr("src", "/lib/barcode.php?text=" + code);
}

function load() {
	
	$('input').each(function(){
		$(this).next('span').text($(this).val());
	});

}