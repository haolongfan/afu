<?php

require_once 'data.php';

$data = new Data();

//$orders = $data->readDataFromFile('orders.csv');
$orders = $data->readDataFromURL('orders');
//$products = $data->readDataFromFile('products');
$products = $data->readDataFromURL('products');
?>

<html>
    
<head>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    
    <link rel="stylesheet" type="text/css" href="style.css">
</head>

<body>

<?php foreach($orders as $order): ?>

<form id="print_form" class="container">

<table id="header">
    <tr>
        <td><h1>中环CHS</br><span>代理号:<?php echo $order['member_number'] ?></span></h1></td>
        <td><img id="barcode" src="/lib/barcode.php?text=<?php echo $order['order_number'] ?>" /></td>
    </tr>
</table>

<fieldset>
<table>
    <tr>
        <td colspan="2">
            <label for="sender_address">寄件人地址：</label>
            <span><?php echo $order['sender_address'] ?></span>
        </td>
    </tr>
    <?php 
    /*
    <tr>
        <td>
            <label for="sender_postcode">邮编：</label>
            <input type="text" id="sender_postcode" name="sender_postcode"/>
        </td>
    </tr> 
    */ 
    ?>
    <tr>
        <td>
            <label for="sender_name">寄件人姓名：</label>
            <span><?php echo $order['sender_name'] ?></span>
        </td>
        <td>
            <label for="sender_phone">电话：</label>
            <span><?php echo $order['sender_phone'] ?></span>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <label for="sender_date">寄件时间：</label>
            <span></span>
        </td>
    </tr>
</table>
</fieldset>

<fieldset>
<table>
    <tr>
        <td colspan="2">
            <label for="receiver_address_1">收件人地址 1：</label>
            <span><?php echo $order['receiver_address_1'] ?></span>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <label for="receiver_address_2">收件人地址 2：</label>
            <span><?php echo $order['receiver_address_2'] ?></span>
        </td>
    </tr>
     <?php 
    /*
    <tr>
        <td>
            <label for="receiver_postcode">邮编：</label>
            <input type="text" id="receiver_postcode" name="receiver_postcode"/>
        </td>
    </tr>
    */
    ?>
    <tr>
        <td>
            <label for="receiver_name">收件人姓名：</label>
            <span><?php echo $order['receiver_name'] ?></span>
        </td> 
        <td>
            <label for="receiver_phone">电话：</label>
            <span><?php echo $order['receiver_phone'] ?></span>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <label for="receiver_licence">身份证号:</label>
            <span><?php echo $order['receiver_licence'] ?></span>
        </td>
    </tr>
</table>
</fieldset>

<fieldset id="products_table">
<table>
    <thead>
        <tr>
            <th style="width: 400px;">货物名称</th>
            <th>单价</th>
            <th>数量</th>
            <th>价值</th>
        </tr>
    </thead>
    
    <?php $products = explode("\n", $order['products']); ?>
    
    <tbody>
        <?php foreach($products as $product): ?>
        
        <?php $product_name = explode("*", $product)[0];
              $product_quantity = intval(explode("*", $product)[1]);
              $product_price = array_search($product_name, array_column($products, 'product_name'));
              
              if(!$product_price) {
                  $product_price = 0;
              }

              $product_value = $product_price * $product_quantity;
        ?>
        <tr>
            <td><span class="product_name"><?php echo $product_name ?></span></td>
            <td><span class="product_price"><?php echo $product_price ?></span></td>
            <td><span class="product_quantity"><?php echo $product_quantity ?></span></td>
            <td><span class="product_value"><?php echo $product_value ?></span><input class="product_value" type="text" min="0" value="<?php echo $product_value ?>" disabled/></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
 </table>
</fieldset>
</form>

<?php endforeach; ?>

</body>