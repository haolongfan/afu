<?php

require_once 'lib/parsecsv.lib.php';

class Data {
    
    
    private $orders_url = "https://docs.google.com/spreadsheets/d/1o18LXYTWWCnaKAbtlJbYE_5SQpPCOllYQLdzML9HGyg/pub?output=csv";
    private $products_url = "https://docs.google.com/spreadsheets/d/1v9M3GR-eTTTIKFnw5PV6hJfUCu0RAIAIPlDc8RDwpW4/pub?output=csv";
    private $receivers_url = "https://docs.google.com/spreadsheets/d/1R31iqt0m27U0rAPef5yI3FX9n3OcHkoSJHndtEaKVu0/pub?output=csv";
    private $senders_url = "https://docs.google.com/spreadsheets/d/1kkXrsaNC4hsJ2B59kONa55MkXxTqvupkxhribuJ21VM/pub?output=csv";
    
    function readDataFromFile($filename){
        $csv = new parseCSV('data/'.$filename.'.csv');
        $csv->encoding('UTF-16', 'UTF-8');
        return $csv->data;
    }
    
    function readDataFromURL($filename){
        
        $url = null;
        
        switch($filename) {
            case 'orders': $url = $this->orders_url; break;
            case 'products': $url = $this->products_url; break;
            case 'receivers': $url = $this->receivers_url; break;
            case 'senders': $url = $this->senders_url; break;
        }
        
        file_put_contents("data/".$filename.'.csv', fopen($url, 'r'));
        return $this->readDataFromFile($filename);
    }
    
}

?>
