<?php

require_once 'data.php';

$data = new Data();

$orders = json_encode($data->readDataFromURL('orders'));
$senders = json_encode($data->readDataFromURL('senders'));
$receivers = json_encode($data->readDataFromURL('receivers'));
$products = json_encode($data->readDataFromURL('products'));

?>

<html>
    
<head>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    
    <script type="text/javascript">
        var orders = <?php echo $orders ?>;
        var senders = <?php echo $senders ?>;
        var receivers = <?php echo $receivers ?>;
        var products = <?php echo $products ?>;
    </script>
    
    <script type="text/javascript" src="js/script.js"></script>
    
    <link rel="stylesheet" type="text/css" href="style.css">
</head>

<body>
    
<form class="container">
    
<table id="header">
    <tr>
        <td><h1>中环CHS</br><span>代理号:2299</span></h1></td>
        <td><img id="barcode" src="https://placeholdit.imgix.net/~text?txtsize=33&txt=350%C3%97150&w=350&h=150" /></td>
    </tr>
</table>


<fieldset id="order_number_wrapper">
<table>
    <tr>
        <td>
            <label for="order_number">单号：</label>
            <input type="text" id="order_number" name="order_number"/>
            <span></span>
        </td>
    </tr>
</table>
</fieldset>

<fieldset>
<table>
    <tr>
        <td colspan="2">
            <label for="sender_address">寄件人地址：</label>
            <input type="text" id="sender_address" name="sender_address"/>
            <span></span>
        </td>
    </tr>
    <?php 
    /*
    <tr>
        <td>
            <label for="sender_postcode">邮编：</label>
            <input type="text" id="sender_postcode" name="sender_postcode"/>
        </td>
    </tr> 
    */ 
    ?>
    <tr>
        <td>
            <label for="sender_name">寄件人姓名：</label>
            <input type="text" id="sender_name" name="sender_name"/>
            <span></span>
        </td>
        <td>
            <label for="sender_phone">电话：</label>
            <input type="tel" id="sender_phone" name="sender_phone"/>
            <span></span>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <label for="sender_date">寄件时间：</label>
            <input type="date" id="sender_date" name="sender_date"/>
            <span></span>
        </td>
    </tr>
</table>
</fieldset>

<fieldset>
<table>
    <tr>
        <td colspan="2">
            <label for="receiver_address_1">收件人地址 1：</label>
            <input type="text" id="receiver_address_1" name="receiver_address_1"/>
            <span></span>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <label for="receiver_address_2">收件人地址 2：</label>
            <input type="text" id="receiver_address_2" name="receiver_address_2"/>
            <span></span>
        </td>
    </tr>
     <?php 
    /*
    <tr>
        <td>
            <label for="receiver_postcode">邮编：</label>
            <input type="text" id="receiver_postcode" name="receiver_postcode"/>
        </td>
    </tr>
    */
    ?>
    <tr>
        <td>
            <label for="receiver_name">收件人姓名：</label>
            <input type="text" id="receiver_name" name="receiver_name"/>
            <span></span>
        </td> 
        <td>
            <label for="receiver_phone">电话：</label>
            <input type="tel" id="receiver_phone" name="receiver_phone"/>
            <span></span>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <label for="receiver_licence">身份证号:</label>
            <input type="text" id="receiver_licence" name="receiver_licence"/>
            <span></span>
        </td>
    </tr>
</table>
</fieldset>

<fieldset id="products_table">
<table>
    <thead>
        <tr>
            <th style="width: 400px;">货物名称</th>
            <th>单价</th>
            <th>数量</th>
            <th>价值</th>
        </tr>
    </thead>
    
    <tbody>
        <tr class="no-display">
            <td><input class="product_name" type="text"/><span class="product_name"></span></td>
            <td><input class="product_price" type="text" min="0"/><span class="product_price"></span></td>
            <td><input class="product_quantity" type="number" min="0"/><span class="product_quantity"></span></td>
            <td><span class="product_value"></span><input class="product_value" type="text" min="0"" disabled/></td>
        </tr>
    </tbody>
 </table>
</fieldset>

<iframe id="printf" src="print.php" ></iframe>

<fieldset id="buttonset">
    
    <button id="btn_print" type="button" onclick="load();window.print();">Print This</button>
    
    <button id="btn_print" type="button" onclick="window.frames[0].print();">Print All Orders</button>
    
</fieldset>

</form>

</body>